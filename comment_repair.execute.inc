<?php
/**
 * @file
 * comment_repair.execute.inc
 */

/**
 * Form callback for executing comment threading repair.
 */
function comment_repair_execute_form() {
  drupal_set_message(t('WARNING: Use with care. Test comment repair on non-production site first.'), 'warning');
  $form['node'] = array(
    '#type' => 'textfield',
    '#title' => t('Content'),
    '#description' => t('Begin typing content title to show autocomplete list.'),
    '#required' => TRUE,
    '#prefix' => '<p>'. t('Repairs comment threading for the specified content.') .'</p>',
    '#autocomplete_path' => 'comment-repair/autocomplete',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Repair Threading'),
  );
  return $form;
}

/**
 * Validation handler for executing comment threading repair.
 */
function comment_repair_execute_form_validate($form, $form_state) {
  preg_match('/\[NID:([0-9]+)\]/i', $form_state['values']['node'], $matches);

  // Check valid content has been selected.
  if (!isset($matches[1]) || !is_numeric($matches[1]) || $matches[1] < 1) {
    form_set_error('node', t('Please choose valid content to repair comment threading.'));
    return;
  }

  // Check content object is loadable.
  $node = node_load($matches[1]);
  if (!$node || !isset($node->nid)) {
    form_set_error('node', t('Please choose valid content to repair comment threading.'));
  }
}

/**
 * Submission handler for executing comment threading repair.
 */
function comment_repair_execute_form_submit($form, $form_state) {
  preg_match('/\[NID:([0-9]+)\]/i', $form_state['values']['node'], $matches);
  $node = node_load($matches[1]);
  $comments = array();

  // Retrieve all comments attached to entity.
  $results = db_select('comment', 'c')
    ->fields('c', array('cid', 'pid', 'created'))
    ->condition('c.nid', $node->nid)
    ->orderBy('c.created', 'ASC')
    ->execute();
  foreach ($results as $data) {
    $comments[] = (array) $data;
  }

  // Collect and calculate new comment threading strings.
  $tree = comment_repair_threading_tree($comments);
  $threads = comment_repair_threading_values($tree);

  // Update comment threads in database table.
  foreach ($threads as $cid => $thread_string) {
    db_update('comment')
      ->fields(array('thread' => $thread_string .'/'))
      ->condition('cid', $cid)
      ->execute();
  }

  // Display success message.
  if (!empty($threads)) {
    drupal_set_message(t('Re-threaded @count total comments in %title.', array(
      '@count' => count($threads),
      '%title' => check_plain($node->title),
    )));
  }
  else {
    drupal_set_message(t('No comments found in selected content.'));
  }
}

/**
 * Creates associative array of threaded comments.
 *
 * @param $comments array
 *   Flat array of comment records.
 * @param $pid int
 *   Parent ID used to recursively create array.
 * @return array
 *   Associative array of threaded comments.
 */
function comment_repair_threading_tree(array $comments, $pid = 0) {
  $branch = array();
  foreach ($comments as $comment) {
    if ($comment['pid'] == $pid) {
      $children = comment_repair_threading_tree($comments, $comment['cid']);
      if ($children) {
        $comment['children'] = $children;
      }
      $branch[] = $comment;
    }
  }
  return $branch;
}

/**
 * Converts threaded comments into associative array of thread strings.
 *
 * @param $tree array
 *   Associtiave array containing comment threading tree.
 * @param $prefix string
 *   Prefix to be prepended to thread strings.
 * @param $threading array
 *   Associative array of existing thread strings.
 * @return array
 *   Accociative array of comment thread strings.
 */
function comment_repair_threading_values($tree, $prefix = '', $threading = array(), $init = TRUE) {
  $thread = $init ? '01' : '00';
  uasort($tree, 'comment_repair_threading_sort');
  foreach ($tree as $comment) {
    $string = (!empty($prefix) ? $prefix .'.' : '') . int2vancode(sprintf('%02d', $thread++));
    $threading[$comment['cid']] = $string;
    if (isset($comment['children'])) {
      $children = $comment['children'];
      uasort($children, 'comment_repair_threading_sort');
      $child_threading = comment_repair_threading_values($children, $string, $threading, FALSE);
      $threading += $child_threading;
    }
  }
  return $threading;
}

/**
 * Sorts associative array of comments by creation time.
 */
function comment_repair_threading_sort($a, $b) {
  if ($a['created'] == $b['created']) {
    return 0;
  }
  return ($a['created'] < $b['created']) ? -1 : 1;
}
